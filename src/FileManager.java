import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileManager {
	BufferedReader br =null;
	FileReader myfile = null;
	
	public Fichier getFile (String fileName){
	Fichier f = new Fichier();
		 try {
	           myfile = new FileReader(fileName);
	            br = new BufferedReader(myfile);
	            String str;

	            while ((str = br.readLine()) != null) {
	               f.addLine(str);
	            }
	        }

	        catch (IOException e) {
	            e.printStackTrace();
	        }

	        finally {

	            try {
	                myfile.close();
	                br.close();
	            }

	            catch (IOException x) {
	                x.printStackTrace();
	            }

	        }
		return f;
	}
	public void printFile (Statistiques s){
		StringBuilder sb =new StringBuilder() ;
		sb.append("Hallo!:").append(System.lineSeparator());
	
		sb.append("NB of lines = "+ s.nbrDeLignes()).append(System.lineSeparator());
		sb.append("NB of words = "+ s.nbrDeMots()).append(System.lineSeparator());
		sb.append("NB of voyels = "+ s.nbrDeVoyelles()).append(System.lineSeparator());
		sb.append("Thank you !").append(System.lineSeparator());
		String message= sb.toString();
		BufferedWriter writer = null;
		try
		{
		    writer = new BufferedWriter( new FileWriter("output.txt"));
		    writer.write(message);
		}
		catch ( IOException e)
		{
		}
		finally
		{
		    try
		    {
		        if ( writer != null)
		        writer.close( );
		    }
		    catch ( IOException e)
		    {
		    }
		}
	}
	
}

