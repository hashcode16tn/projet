import java.util.ArrayList;

class Statistiques {

	ArrayList<String> voyelles;
	Fichier fich;
	String[] tableau;

	public Statistiques(Fichier fich) {
		String[] voy = { "a", "e", "y", "u", "i", "o" };
		voyelles = new ArrayList<String>();
		for (String i : voy)
			voyelles.add(i);

		this.fich = fich;

		// filling the array
		tableau = new String[fich.nbrOfLines()];
		for (int i = 0; i < fich.nbrOfLines(); i++)
			tableau[i] = fich.getLine(i);
	}

	private int nbrDeMotsDansUneLigne(String chaine) {
		return chaine.split(" ").length + 1;
	}

	public int nbrDeMots() {
		int nbr = 0;
		for (String chaine : tableau)
			nbr += nbrDeMotsDansUneLigne(chaine);
		return nbr;
	}

	public int nbrDeLignes() {
		return tableau.length;
	}

	private int nbrDeVoyellesDansUneLigne(String chaine) {
		int nbr = 0;

		for (int i = 0; i < chaine.length(); i++)
			if (voyelles.contains(new Character(chaine.charAt(i)).toString()))
				nbr += 1;
		return nbr;
	}

	public int nbrDeVoyelles() {
		int nbr = 0;
		for (String chaine : tableau)
			nbr += nbrDeVoyellesDansUneLigne(chaine);

		return nbr;
	}
}
