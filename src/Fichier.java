import java.util.ArrayList;

public class Fichier {
	ArrayList<String> lines=new ArrayList<String>();

	public void addLine(String l) {
		lines.add(l);
	}

	public String getLine(int i) {
		return lines.get(i);
	}

	public int nbrOfLines() {
		return lines.size();
	}
}
